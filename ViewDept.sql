CREATE VIEW dept AS
   SELECT Department.Dname,Dept_Locations.LocationName
FROM Department
LEFT JOIN Dept_Locations
ON Department.Location_id=Dept_Locations.Lnumber
ORDER BY Department.ManagerID ASC;
        
